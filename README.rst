pandasModel
===========
IO optimizations over Pandas.

Cuurently empty project (idea placeholder).

Just pandas.DataFrame optimizations of:

* Lazy loaded columns: compute just once "always again" re-computed data.
* IO optimizations: some columns are coerced to broader data-types. But once you save them, e.g. int16 is enought.
* Lazy load DataFrame behind the scene in chunked way by default. E.g. openpyxl DatFrame loading is one-shot synchronous. This is trouble if big inside.
* Lazy load DataFrame - initialize /w pre-set ways to load from the backend (SQL, pickle, csv, ...).

This project was started during leaving Solargis.com . I had never time to try to implement this, so trying now.
